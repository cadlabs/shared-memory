# README #

CAD repository for shared memory labs

### How do I get set up? ###

This is a Cmake project.

In a IDE with Cmake support simply compile the project and run the GoogleTest tests.

If you prefer to use the command line, follow the following steps:

- go to the project's folder

- create a build folder: mkdir build

- cd build

- cmake ..

- make

- run tests: ./tests/cadlabtests_*


### Who do I talk to? ###

herve.paulino@fct.unl.pt